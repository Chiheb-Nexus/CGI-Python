#!/usr/bin/python3

"""
####################################################################################
# Warning!!: This example can be hakched easly ! Do not upload it in your servers!
#            This example should be used in local machine for education purpose!
#
# Description: This is a CGI-Python who return /bin/bash outputs in html format
#              This is can be useful for debuggin servers or for "Hacking" (0~0")
#
# Author = Chiheb NeXus && Python Tutorials
# Licence = GPLv3
# Blog = nexus-coding.blogspot.com
#
####################################################################################
"""
import cgitb; cgitb.enable()

import os, subprocess as sub
from urllib.parse import unquote

# TODO:
#      - Generate a dynamic prohibed list than can handle all the keyword combinations of commands that can hurt servers
#       or commands that can upload/download files on servers.
#      - Give the output an awsome look ! (Why not PHP && CSS ??)
# NOTE: Files have 755 permissions

# Retrieve the command from the query string
# and unencode the escaped %xx chars

prohibed= ["git", "git --help", "git --h", "wget", "wget --help", "wget --h", "alias", "alias --help"]

if unquote(os.environ['QUERY_STRING']) in prohibed :
    str_command = "Error!"
else:
    str_command = unquote(os.environ['QUERY_STRING'])

p = sub.Popen(['/bin/bash', '-c', str_command],
    stdout=sub.PIPE, stderr=sub.STDOUT)

try:
    output = unquote(p.stdout.read().decode("ascii"))
except:
    output = "This command is prohibed!"

print ("""\
Content-Type: text/html\n
<html>
<head>
<title> CGI-Python Example </title>
</head>
<body>
<pre>
<center><h1>Hello, this is a CGI-Python who output /bin/bash commands</h1></center>
> %s
<b><font color="blue">%s</font></b>
</pre>
</body></html>
""" % (str_command, output))
